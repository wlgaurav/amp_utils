package networking.model;

import java.util.List;
import java.util.Map;

public class Response {

	public int status;
	public Map<String, List<String>> headers;
	public String body;
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("");
		sb.append("status = " + status);
		sb.append(headers!=null ? ("\n" + "headers = " + headers.toString()) : "");
		sb.append(body!=null ? ("\n" + "body = " + body):"");
		return sb.toString();
	}
	
	public boolean isSuccess(){
		return status==200;
	}
}
