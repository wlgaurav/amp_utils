package networking.model;

import com.google.gson.JsonObject;

public class Request {

	public String url;
	public JsonObject headers;
	public String body;
	
	public Request(String url){
		super();
		this.url = url;
	}

	public Request(String url, JsonObject headers, String body) {
		super();
		this.url = url;
		this.headers = headers;
		this.body = body;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("");
		sb.append("url = " + url);
		sb.append(headers!=null ? ("\n" + "headers = " + headers.toString()) : "");
		sb.append(body!=null ? ("\n" + "body = " + body):"");
		return sb.toString();
	}
}
