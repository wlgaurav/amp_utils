package networking;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map.Entry;

import com.google.gson.JsonElement;

import logging.Logger;
import networking.model.Request;
import networking.model.Response;

public class Networker {

	private static void debug(String msg) {
		Logger.debug(msg);
	}

	private static void error(String msg) {
		Logger.error(msg);
	}

	public static Response dummy(final Request request) {
		debug("#request\n" + request);
		return null;
	}

	public static Response get(final Request request) throws Exception {

		debug("#request\n" + request);
		Response result = new Response();

		HttpURLConnection con = getConnection(request);
		con.setRequestMethod("GET");

		int responseCode = con.getResponseCode();
		result.status = responseCode;

		if (responseCode == HttpURLConnection.HTTP_OK) { // success
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			result.body = response.toString();
			if (result.body.contains("Notice")) {
				error("#response\n" + result);
			} else {
				debug("#response\n" + result);
			}
		} else {
			error("#response\n" + result);
		}

		return result;
	}

	public static Response post(final Request request) throws Exception {

		debug("#request\n" + request);
		Response result = new Response();

		HttpURLConnection con = getConnection(request);
		con.setRequestMethod("POST");

		con.setDoOutput(true);
		if (request.body != null) {
			OutputStream os = con.getOutputStream();
			os.write(request.body.getBytes());
			os.flush();
			os.close();
		}

		int responseCode = con.getResponseCode();
		result.status = responseCode;

		if (responseCode == HttpURLConnection.HTTP_OK) { // success

			result.headers = con.getHeaderFields();

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			result.body = response.toString();
			debug("#response\n" + result);
		} else {
			error("#response\n" + result);
		}

		return result;
	}

	private static HttpURLConnection getConnection(Request request) throws IOException {
		URL obj = new URL(request.url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		if (request.headers != null) {
			for (Entry<String, JsonElement> entry : request.headers.entrySet()) {
				String key = entry.getKey();
				String value = entry.getValue().getAsString();
				con.setRequestProperty(key, value);
			}
		}
		return con;
	}
}
