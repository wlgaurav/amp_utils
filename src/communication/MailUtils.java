package communication;

import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import logging.Logger;

public class MailUtils {

	private static String host;
	private static String port;
	private static String username;
	private static String password;

	private static boolean isInitiated;

	public static boolean isLogging = false;

	private static void print(String msg) {
		if (isLogging)
			Logger.info(msg);
	}

	public static void init(String host, String port, String username, String password) {
		MailUtils.host = host;
		MailUtils.port = port;
		MailUtils.username = username;
		MailUtils.password = password;
		MailUtils.isInitiated = true;
	}

	public static void sendMail(String from, String[] to, String[] cc, String subject, String body, String[] filePaths)
			throws Exception {

		if (!isInitiated) {
			Logger.warning("mail utils not initiated, can't send mail!");
			return;
		}

		Properties properties = new Properties();
		properties.put("mail.transport.protocol", "smtp");
		properties.put("mail.smtp.host", host);
		properties.put("mail.smtp.port", port);
		properties.put("mail.smtp.auth", "true");

		Authenticator auth = new SMTPAuthenticator();
		Session mailSession = Session.getDefaultInstance(properties, auth);

		MimeMessage message = new MimeMessage(mailSession);
		Multipart multipart = new MimeMultipart("alternative");

		if (body != null) {
			BodyPart part2 = new MimeBodyPart();
			part2.setContent(body, "text/html");
			multipart.addBodyPart(part2);
		}

		message.setFrom(new InternetAddress(from));

		for (String email : to) {
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(email));
		}

		if (cc != null) {
			for (String email : cc) {
				message.addRecipient(Message.RecipientType.CC, new InternetAddress(email));
			}
		}
		message.setSubject(subject);
		message.setContent(multipart);

		InternetAddress addresses[] = { new InternetAddress(from) };

		if (filePaths != null) {
			for (String filePath : filePaths) {
				if (filePath != null) {
					MimeBodyPart attachmentPart = new MimeBodyPart();
					DataSource source = new FileDataSource(filePath);
					attachmentPart.setDataHandler(new DataHandler(source));
					String fileName = filePath.substring(filePath.lastIndexOf("/") + 1);
					print("attaching to mail, path: " + filePath + " name: " + fileName);
					attachmentPart.setFileName(fileName);
					multipart.addBodyPart(attachmentPart);
				}
			}
		}

		message.setReplyTo(addresses);

		Transport transport = mailSession.getTransport();
		transport.connect();
		transport.sendMessage(message, message.getAllRecipients());
		transport.close();

		print("mail sent");
	}

	private static class SMTPAuthenticator extends javax.mail.Authenticator {
		public PasswordAuthentication getPasswordAuthentication() {
			return new PasswordAuthentication(username, password);
		}
	}

}
