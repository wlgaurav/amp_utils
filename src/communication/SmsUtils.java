package communication;

import java.net.URLEncoder;

import logging.Logger;
import networking.Networker;
import networking.model.Request;
import networking.model.Response;

public class SmsUtils {

	private static String hostUrl;
	private static String mobileKey;
	private static String msgKey;

	private static boolean isInitiated;

	public static boolean isLogging = false;

	private static void print(String msg) {
		if (isLogging)
			Logger.info(msg);
	}

	public static void init(String hostUrl, String mobileKey, String msgKey) {
		SmsUtils.hostUrl = hostUrl;
		SmsUtils.mobileKey = mobileKey;
		SmsUtils.msgKey = msgKey;
		SmsUtils.isInitiated = true;
	}

	public static int sendSms(String[] msisdns, String msg)
			throws Exception {

		if (!isInitiated) {
			Logger.warning("smsutils not initiated, can't send sms!");
			return 0;
		}
		
		int sent=0;
		String url = hostUrl.replaceAll(msgKey, URLEncoder.encode(msg));
		for(String msisdn: msisdns){
			Request request = new Request(url.replaceAll(mobileKey, msisdn));
			Response response = Networker.get(request);
			if(response!=null && response.isSuccess()){
				print("sms success: " + msisdn);
				sent++;
			}else{
				print("sms failure: " + msisdn);
			}
		}
		
		return sent;
	}
	
	public static boolean sendSms(String msisdn, String msg) throws Exception{
		
		if (!isInitiated) {
			Logger.warning("smsutils not initiated, can't send sms!");
			return false;
		}
		
		String url = hostUrl.replaceAll(msgKey, URLEncoder.encode(msg));
		Request request = new Request(url.replaceAll(mobileKey, msisdn));
		Response response = Networker.get(request);
		if(response!=null && response.isSuccess()){
			print("sms success: " + msisdn);
			return true;
		}else{
			print("sms failure: " + msisdn);
			return false;
		}
	}
}
