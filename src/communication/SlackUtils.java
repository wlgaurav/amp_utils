package communication;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import logging.Logger;
import networking.Networker;
import networking.model.Request;
import networking.model.Response;

public class SlackUtils {

	private static String hook;
	private static boolean isInitiated;

	public static boolean isLogging = false;

	private static void print(String msg) {
		if (isLogging)
			Logger.info(msg);
	}

	public static void init(String url) {
		SlackUtils.hook = url;
		SlackUtils.isInitiated = true;
	}
	
	public static void postMsg(String title, String content)
			throws Exception {
		if (!isInitiated) {
			Logger.warning("slack utils not initiated, can't post msgs!");
			return;
		}
		Request request = new Request(hook);
		request.body = getBody(title, content);
		post(request);
	}

	public static void postMsg(String title, String[] fileNames, String[] filePaths)
			throws Exception {
		if (!isInitiated) {
			Logger.warning("slack utils not initiated, can't post msgs!");
			return;
		}
		Request request = new Request(hook);
		request.body = getBody(title, fileNames, filePaths);
		post(request);
	}
	
	private static void post(Request request) throws Exception{
		Response response = Networker.post(request);
		if(response.isSuccess()){
			print("msg posted");
		}else{
			print("msg posting failed");
		}
	}

	private static String getBody(String title, String[] fileNames, String[] filePaths) {
		JsonObject json = new JsonObject();
		json.addProperty("text", title);
		JsonArray array = new JsonArray();
		for(int i=0; i<fileNames.length; i++){
			String fileName = fileNames[i]; 
			String filePath = filePaths[i];
			JsonObject r = new JsonObject();
			r.addProperty("pretext", fileName + ": " + filePath);
			array.add(r);
		}
		json.add("attachments", array);
		String body = json.toString();
		print("slack body: " + body);
		return body;
	}
	
	private static String getBody(String title, String content) {
		JsonObject json = new JsonObject();
		json.addProperty("text", title);
		json.addProperty("text", content);
		String body = json.toString();
		print("slack body: " + body);
		return body;
	}
}
