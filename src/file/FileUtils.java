package file;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class FileUtils {

	public static boolean isLogging = false;

	private static void print(String msg) {
		if (isLogging)
			System.out.println(msg);
	}

	public static void write(String filepath, String contents, boolean isAppend) throws IOException {
		File file = new File(filepath);
		FileWriter writer = new FileWriter(file, isAppend);
		print("file writing started: " + filepath);
		BufferedWriter bw = new BufferedWriter(writer);
		bw.write(contents);
		bw.close();
		writer.close();
		print("file writing finished: " + filepath);
	}

	public static boolean doesFileExist(String filePath) {
		return new File(filePath).exists();
	}
}
