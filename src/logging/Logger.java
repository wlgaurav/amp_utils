package logging;

import java.util.Date;

public class Logger {
	
	public static int LEVEL = 5;

	public static void error(String str) {
		if(LEVEL>0)
			System.err.println(new Date() + " error: " + str);
	}
	
	public static void warning(String str) {
		if(LEVEL>1)
			System.err.println(new Date() + " warning: " + str);
	}
	
	public static void info(String str) {
		if(LEVEL>2)
			System.out.println(new Date() + " info: " + str);
	}
	
	public static void debug(String str) {
		if(LEVEL>3)
			System.out.println(new Date() + " debug: " + str);
	}
	
	public static void trace(String str) {
		if(LEVEL>4)
			System.out.println(new Date() + " trace: " + str);
	}
}
