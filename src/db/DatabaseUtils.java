package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import file.FileUtils;
import logging.Logger;

public class DatabaseUtils {

	public static boolean isLogging = false;

	private static void debug(String msg) {
		if (isLogging)
			Logger.debug(msg);
	}

	private static void warning(String msg) {
		if (isLogging)
			Logger.warning(msg);
	}

	private static void error(String msg) {
		if (isLogging)
			Logger.error(msg);
	}

	private static final String CHAR_NEWLINE = System.getProperty("line.separator");

	private static boolean initiated;
	private static Connection con;

	private static String host;
	private static String port;
	private static String username;
	private static String password;
	private static String db;
	private static String driver;

	public static void init(String host, String port, String username, String password, String db, String driver) {
		if (initiated) {
			warning("DatabaseUtils is already initiated!");
		}
		DatabaseUtils.host = host;
		DatabaseUtils.port = port;
		DatabaseUtils.username = username;
		DatabaseUtils.password = password;
		DatabaseUtils.db = db;
		DatabaseUtils.driver = driver;
		initiated = true;
		getConnection();
		if(con!=null){
			debug("DabaseUtils is initiated now!");
		}
	}

	private static void getConnection() {
		if (con != null) {
			return;
		}
		if (initiated == false) {
			return;
		}
		try {
			Class.forName(driver);
		} catch (ClassNotFoundException e) {
			error(e.getMessage());
		}
		
		String dbType;
		switch(driver){
		case "org.postgresql.Driver":
			dbType = "postgresql";
			break;
		default:
			dbType = "mysql";
		}
		
		String dbUrl = "jdbc:" + dbType + "://" + host + "/" + db;
		Logger.debug("db url: " + dbUrl);
		
		try {
			Properties props = new Properties();
			props.setProperty("user", username);
			props.setProperty("password", password);
//			props.setProperty("ssl","true");
			con = DriverManager.getConnection(dbUrl, props);
		} catch (SQLException e) {
			error(e.getMessage());
		}
	}

	public static void executeWithoutResult(String query) throws Exception {

		getConnection();
		debug("db: hitting query: " + query);
		Statement st = con.createStatement();
		st.execute(query);
		st.close();
	}

	public static void executeWithoutResult(String[] queries) throws Exception {

		getConnection();
		Statement st = con.createStatement();
		for (String each : queries) {
			if(each!=null){
				debug("db: adding query to batch: " + each);
				st.addBatch(each);
			}
		}
		st.executeBatch();
		st.close();
	}

	public static List<String[]> executeAndGetResultAsList(final String query) throws Exception {

		getConnection();
		Statement st = con.createStatement();
		debug("db: hitting query: " + query);
		ResultSet rs = st.executeQuery(query);
		ResultSetMetaData rsmd = rs.getMetaData();
		int num = rsmd.getColumnCount();
		debug("db: got results, now processing");
		List<String[]> list = new ArrayList<String[]>();
		while (rs.next()) {
			int j = 1;
			String[] array = new String[num];
			while (j <= num) {
				String x = String.valueOf(rs.getObject(j));
				array[j - 1] = x;
				j++;
			}
			list.add(array);
		}
		return list;
	}

	public static String execute(final String query) throws Exception {

		getConnection();
		Statement st = con.createStatement();
		debug("db: hitting query: " + query);
		ResultSet rs = st.executeQuery(query);
		ResultSetMetaData rsmd = rs.getMetaData();
		int num = rsmd.getColumnCount();
		int i = 1;
		StringBuilder sb = new StringBuilder();
		while (i <= num) {
			sb.append(rsmd.getColumnLabel(i));
			if (i != num) {
				sb.append(",");
			}
			i++;
		}
		sb.append(CHAR_NEWLINE);
		debug("db: got results, now processing");

		while (rs.next()) {
			int j = 1;
			while (j <= num) {
				String x = String.valueOf(rs.getObject(j));
				sb.append(x.replace(',', ';'));
				if (j != num) {
					sb.append(",");
				}
				j++;
			}
			sb.append(CHAR_NEWLINE);
		}

		return sb.toString();
	}

	public static String executeWithoutHeaders(final String query) throws Exception {

		getConnection();
		Statement st = con.createStatement();
		debug("db: hitting query: " + query);
		ResultSet rs = st.executeQuery(query);
		ResultSetMetaData rsmd = rs.getMetaData();
		int num = rsmd.getColumnCount();
		StringBuilder sb = new StringBuilder();
		debug("db: got results, now processing");

		while (rs.next()) {
			int j = 1;
			while (j <= num) {
				String x = String.valueOf(rs.getObject(j));
				sb.append(x.replace(',', ';'));
				if (j != num) {
					sb.append(",");
				}
				j++;
			}
			sb.append(CHAR_NEWLINE);
		}

		return sb.toString();
	}

	public static void executeAndWriteFile(final String query, final String filepath) throws Exception {

		getConnection();
		Statement st = con.createStatement();
		debug("db: hitting query: " + query);
		ResultSet rs = st.executeQuery(query);
		debug("db: got results, now processing");

		ResultSetMetaData rsmd = rs.getMetaData();
		int num = rsmd.getColumnCount();
		StringBuilder sb = new StringBuilder("");
		int i = 1;
		while (i <= num) {
			sb.append(rsmd.getColumnLabel(i));
			if (i != num) {
				sb.append(",");
			}
			i++;
		}
		sb.append(CHAR_NEWLINE);
		FileUtils.write(filepath, sb.toString(), false);

		sb.setLength(0);

		int counter = 0;
		while (rs.next()) {
			int j = 1;
			while (j <= num) {
				String x = String.valueOf(rs.getObject(j));
				sb.append(x.replace(',', ';'));
				if (j != num) {
					sb.append(",");
				}
				j++;
			}
			sb.append(CHAR_NEWLINE);
			counter++;
			if (counter % 500 == 0) {
				FileUtils.write(filepath, sb.toString(), true);
				sb.setLength(0);
			}
		}

		FileUtils.write(filepath, sb.toString(), true);
	}
}
