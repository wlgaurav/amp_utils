package formatting;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import constants.Constants;

public class CsvFormatter {

	public static String toHtml(String header, String filePath) throws FileNotFoundException {

		StringBuilder sb = new StringBuilder();
		sb.append("<table border=1px style='border-collapse: collapse; border-spacing: 0; color: #333; font-family: Helvetica, Arial, sans-serif;'>");
		sb.append("<tr><th colspan=100 style='background: #DFDFDF; font-weight: bold; border: 1px solid; height: 30px; padding-left: 10px; padding-right: 10px;'>");
		sb.append(header+Constants.EOL_HTML);
		sb.append("</th></tr>");

		int count = 0;
		Scanner s = new Scanner(new File(filePath));
		while (s.hasNextLine()) {
			String line = s.nextLine();
			String[] values = line.split(",");
			if(count%2==0){
				sb.append("<tr style='background: #F1F1F1;'>");
			}else{
				sb.append("<tr style='background: #FEFEFE;'>");
			}
			for (String each : values) {
				if (count == 0) {
					sb.append("<th align='left' style='border: 1px solid; height: 30px; padding-left: 10px; padding-right: 10px;'>");
					sb.append(each);
					sb.append("</th>");
				} else {
					sb.append("<td style='border: 1px solid; height: 30px; padding-left: 10px; padding-right: 10px;'>");
					sb.append(each);
					sb.append("</td>");
				}
			}
			sb.append("</tr>");
			count++;
		}
		sb.append("</table>");
		s.close();
		return sb.toString();
	}

	public static String toSlack(String header, String filePath) throws FileNotFoundException {

		StringBuilder sb = new StringBuilder();
		sb.append("<table border=1>");
		sb.append("<tr><th colspan=100>");
		sb.append("<b>" + header + "</b>" + Constants.EOL);
		sb.append("</th></tr>");
		Scanner s = new Scanner(new File(filePath));
		while (s.hasNextLine()) {
			String line = s.nextLine();
			String[] values = line.split(",");
			sb.append("<tr>");
			for (String each : values) {
				sb.append("<td>");
				sb.append(each);
				sb.append("</td>");
			}
			sb.append("</tr>");
		}
		sb.append("</table>");
		s.close();
		return sb.toString();
	}
}
